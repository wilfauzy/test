---
title: rabu
date: 2020-10-27T17:00:00.000+00:00
tags:
- coretan
author: Idan
featuredimg: https://images.unsplash.com/photo-1566224425427-998503a013f6?ixlib=rb-1.2.1&auto=format&fit=crop&w=898&q=80
summary: mata perih oleh serpihan kaca yang pecah

---
entah mengapa hari ini sejak pergantian hari, rasanya seperti dihantam nuklir buatan cina, gelap dan sesak, tersisa teruntuhan, mungkin karena tidak ada lagi rabu dan sabtu.

mata perih oleh serpihan kaca yang pecah, kelenjar kantung mata sudah mengering, dihisap oleh awan, agar besok bisa hujan tapi itu nanti.

hal yang aneh serangan nuklir menancap di tengah-tengah dada, tidak ada yang bisa mengangkat, seperti tertanam di dasar bumi.