---
title: jumat
date: 2020-10-30T05:00:00.000+00:00
tags:
- coretan
author: Idan
featuredimg: https://images.unsplash.com/photo-1523978591478-c753949ff840?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjExMzk2fQ&auto=format&fit=crop&w=1350&q=80
summary: keluar rumah iseng-iseng apakah warkop masih buka, ternyata masih ramai

---
bisa dibilang malam jumat sama kedudukannya dengan malam minggu, sebab tidak semua libur pada hari minggu, rata-rata hari jumat adalah hari libur kalau disini.

malam yang panjang, daerah kuburan pun ramai, banyak orang berziarah atau berjualan berbagai macam aneka barang dan makanan.

begitu pula dengan berkumpul orang yang kesepian, sudah jam setengah dua belas malam, terasa hampar jika hanya ditemani seekor cicak yang buntung.

keluar rumah iseng-iseng apakah warkop masih buka, ternyata masih ramai, beberapa orang tidak mengenakan baju karena kegerahan tapi selalu mengeluh digigit nyamuk, akhirnya mandi dengan autan.

memesan secangkir kopi dan satu gelas mineral, tapi sebelumnya menunggu mie indomie empal gentong plus telur setengah matang, sehabis makan tunggu beberapa menit baru ngopi.

setengah jam kemudian perut mules, tidak ada salahnya tidur, ketika melihat jam sudah pukul enam pagi, lalu kaget terbangun cuaca gelap hujan besar dikira sudah sore, pas lihat jam masih pukul sembilan sialan.