---
title: minggu
date: 2020-11-01 05:00:00 +0000
tags:
- coretan
author: Idan
featuredimg: https://images.unsplash.com/photo-1464052259809-c19ec2ddfc5f?ixlib=rb-1.2.1&auto=format&fit=crop&w=1355&q=80
summary: ketika mau tidur sehabis subuh, pintu terketok

---
baru pertama kali tidur seaneh ini, cuaca mulai mendung sehabis ashar, sebelum orang-orang mulai pulang dari rumah, sesudah makan sejak tadi pagi buta.

harus diakui bahwa manusia tanpa tidur seharian adalah manusia titisan kelelawar dan berdarah air accu, semalam begadang seperti biasanya.

ketika mau tidur sehabis subuh, pintu terketok, baru teringat seorang teman ingin mampir pas sehabis magrib tapi baru datang sehabis subuh.

niat awal ingin tidur subuh tidak terlaksana, mengobrol panjang lebar tentang keluh kesah kehidupan yang semakin asudahlah.

sampai hari mulai sore baru pada pulang, baru ketika sendiri kembali rasa ingin tidur sangat tinggi, menyalakan semua lampu, mengunci pintu dan mulai tidur.

mimpi di waktu tidur yang aneh memang benar-benar aneh, tidak seperti mimpi pada biasanya, kejadian yang terulang di kehidupan nyata terjadi di mimpi seperti reka adegan.

terbangun gara-gara suara kucing lewat diatas atap kamar, dan melihat jam sudah pukul setengah malam, keadaan masih ramai, pesan instan sudah menumpuk, meminum air putih.