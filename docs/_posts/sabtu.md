---
title: sabtu
date: 2020-10-31 05:00:00 +0000
tags:
- coretan
author: Idan
featuredimg: https://images.unsplash.com/photo-1501631957818-9f4b96ca2b0f?ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80
summary: musim hujan mempunyai cerita sendiri, setelah tiga hari tidak bisa tidur

---
musim hujan mempunyai cerita sendiri, setelah tiga hari tidak bisa tidur, atau lebih tepatnya hanya tidur satu dua jam, sebenarnya bingung apa yang dikerjakan ketika tidak tidur sampai beberapa hari.

terbangun dengan keadaan yang sudah gelap, sudah bangun tapi tidur lagi, melewati batas normal manusia pada umumnya, 8 jam sehari tapi dikali dua.

untuk meminimalisir sakit kepala, ketika bangun tidur langsung mengguyur kepala dengan air yang mengalir, dan minum air segar dua gelas serta berjalan kecil.

tapi saya hanya manusia biasa, rasa sakit kepala tidak bisa dihindarkan, langsung keramas, tidak begitu berhasil, akhirnya membuat minuman manis dicampur es, rasa sakit kepala mulai hilang.