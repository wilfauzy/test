module.exports = {
  title: 'Aidan Blog',
  base: '/',
  description: 'aidan panggil saja idan merupakan blog pribadi wildan fauzy kumpulan tulisan coretan.',
  logo: './assets/img/logo.png',
  theme: require.resolve('../../'),
  themeConfig: {
  authors: [
      {
      name: 'Idan',
      avatar: '/assets/img/sal.jpg',
      link: 'https://twitter.com/wilfauzy',
      linktext: 'Follow',
      },
      {
        name: 'John Doe',
        avatar: '/assets/img/avatar.png',
        link: 'https://bootstrapstarter.com/',
        linktext: 'Follow',
      },
    ],
    footer: {
      contact: [
        {
          type: 'codepen',
          link: '#',
        },
        {
          type: 'facebook',
          link: 'https://www.facebook.com/wilfauzy/',
        },
        {
          type: 'github',
          link: 'https://github.com/wilfauzy',
        },
        {
          type: 'gitlab',
          link: '#',
        },
        {
          type: 'instagram',
          link: '#',
        },
        {
          type: 'linkedin',
          link: '#',
        },
        {
          type: 'mail',
          link: '#',
        },
        {
          type: 'messenger',
          link: '#',
        },
        {
          type: 'phone',
          link: '#',
        },
        {
          type: 'twitter',
          link: 'https://twitter.com/wilfauzy',
        },
        {
          type: 'web',
          link: '#',
        }
      ],
      copyright: [
        {
          text: 'Licensed MIT.',
          link: '/',
        },
        {
          text: 'Made with love in Bandung City',
          link: 'https://wildanfauzy.com/',
        },
      ],
    },

    sitemap: {
      hostname: 'https://github.com/wilfauzy/test/'
    },
    comment: {
      service: 'disqus',
      shortname: 'idanblog',
    },
    newsletter: {
      endpoint: 'https://mailchi.mp/311fb8ab9134/blog'
    },
    feed: {
      canonical_base: 'https://github.com/wilfauzy/test/',
    },
    smoothScroll: true
  },
}
